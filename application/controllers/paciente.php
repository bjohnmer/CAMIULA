<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paciente extends CI_Controller {
  
  function __construct()
	{
		
    parent::__construct();

    $this->load->library('session');
    if (!$this->session->userdata("logged_in")){
      redirect('/');
    }
    $this->load->library('grocery_CRUD');

  }
	
	public function index($data = null)
	{

    $crud = new grocery_CRUD();

    // Cnfigurar tabla de trabajo
    $crud->set_table('tbl_pacientes');

    // Inhabilitar el Editor de Texto enrriquecido para los textareas
    $crud->unset_texteditor("direccion_paciente","direccionLaboral_paciente","direccionFamiliar_paciente");

    // // Configurar el sujeto
    $crud->set_subject('Paciente');

    // // Quitar el Id de la tabla
    $crud->unset_fields("id_paciente","fechaCreacion_paciente","fechaActualizacion_paciente");

    // Arreglar los nombres de los campos
    $crud->display_as('historia_paciente', '# de Historia');
    $crud->display_as('cedula_paciente', 'Cédula');
    $crud->display_as('nombre_paciente', 'Nombre');
    $crud->display_as('apellido_paciente', 'Apellido');
    $crud->display_as('direccion_paciente', 'Dirección');
    $crud->display_as('sexo_paciente', 'Sexo');
    $crud->display_as('lugarNacimiento_paciente', 'Lugar de Nacimiento');
    $crud->display_as('fechaNacimiento_paciente', 'Fecha de Nacimiento');
    $crud->display_as('edoCivil_paciente', 'Estado Civil');
    $crud->display_as('grupoSanguineo_paciente', 'Grupo Sanguíneo');
    $crud->display_as('religion_paciente', 'Religión');
    $crud->display_as('claseEconomica_paciente', 'Clase Económica');
    $crud->display_as('profesion_paciente', 'Profesión');
    $crud->display_as('ocupacion_paciente', 'Ocupación');
    $crud->display_as('direccionLaboral_paciente', 'Dirección Laboral');
    $crud->display_as('nombreFamiliar_paciente', 'Nombre de un Familiar o Pariente más cercano');
    $crud->display_as('parentescoFamiliar_paciente', 'Parentesco');
    $crud->display_as('direccionFamiliar_paciente', 'Dirección del Familiar');
    $crud->display_as('fechaEntrevista_paciente', 'Fecha de Entrevista');
    $crud->display_as('nombreEntrevistador_paciente', 'Nombre del Entrevistador');
    $crud->display_as('estado_paciente', 'Estatus del Paciente');
    
    // Configurar las columnas que queremos que se muestren en la lista
    $crud->columns('historia_paciente','cedula_paciente', 'nombre_paciente', 'apellido_paciente', 'sexo_paciente', 'claseEconomica_paciente', 'estado_paciente');
    // $crud->edit_fields('descrip_carrera');

    // Orden de la lista
    $crud->order_by('historia_paciente','ASC');

    // Validaciones
    $crud->set_rules('historia_paciente', '# de Historia',"required");
    $crud->set_rules('cedula_paciente', 'Cédula',"required");
    $crud->set_rules('nombre_paciente', 'Nombre',"required|alpha_space|max_length[50]|min_length[2]");
    $crud->set_rules('apellido_paciente', 'Apellido',"required|alpha_space|max_length[50]|min_length[2]");
    $crud->set_rules('direccion_paciente', 'Dirección', "required|min_length[5]");
    $crud->set_rules('sexo_paciente', 'Sexo',"required");
    $crud->set_rules('lugarNacimiento_paciente', 'Lugar de Nacimiento',"required");
    $crud->set_rules('fechaNacimiento_paciente', 'Fecha de Nacimiento',"required");
    $crud->set_rules('edoCivil_paciente', 'Estado Civil',"required");
    $crud->set_rules('grupoSanguineo_paciente', 'Grupo Sanguíneo');
    $crud->set_rules('religion_paciente', 'Religión');
    $crud->set_rules('claseEconomica_paciente', 'Clase Económica',"required");
    $crud->set_rules('nombreFamiliar_paciente', 'Nombre de un Familiar o Pariente más cercano',"required|alpha_space");
    $crud->set_rules('parentescoFamiliar_paciente', 'Parentesco',"required|alpha_space");
    $crud->set_rules('direccionFamiliar_paciente', 'Dirección del Familiar',"required|min_length[5]");
    $crud->set_rules('fechaEntrevista_paciente', 'Fecha de Entrevista',"required");
    $crud->set_rules('nombreEntrevistador_paciente', 'Nombre del Entrevistador',"required|alpha_space");
    $crud->set_rules('estado_paciente', 'Estatus del Paciente',"required");

    // // Renderiza la Vista
    $salida = $crud->render();
    
    // // Llama a la función que va a mostrar la Vista
    // $this->salida($output);
      
    $this->load->view('header_admin',$salida);
    $this->load->view('pacientes',$salida);
    $this->load->view('footer');
    
	}

}


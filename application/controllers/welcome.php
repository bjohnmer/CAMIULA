<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
function __construct()
	{
		parent::__construct();
    $this->load->library("form_validation");
		
	}
	
	public function index()
	{
    $this->load->view('header');
    $this->load->view('welcome_message');
		$this->load->view('footer');
	}
	public function about()
	{
    $this->load->view('header');
    $this->load->view('about');
		$this->load->view('footer');
	}
  public function contactos()
  {
    $this->load->view('header');
    $this->load->view('contactos');
    $this->load->view('footer');
  }
}


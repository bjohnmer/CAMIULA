<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Especialidad extends CI_Controller {
  
  function __construct()
	{
		
    parent::__construct();

    $this->load->library('session');
    if (!$this->session->userdata("logged_in")){
      redirect('/');
    }
    $this->load->library('grocery_CRUD');

  }
	
	public function index($data = null)
	{

    $crud = new grocery_CRUD();

    // Cnfigurar tabla de trabajo
    $crud->set_table('tbl_especialidad');

    // Inhabilitar el Editor de Texto enrriquecido para los textareas
    //$crud->unset_texteditor("direccion_paciente","direccionLaboral_paciente","direccionFamiliar_paciente");

    // // Configurar el sujeto
    $crud->set_subject('Especialidad');

    // // Quitar el Id de la tabla
    $crud->unset_fields("fechaCreacion_especialidad","fechaActualizacion_especialidad");

    // Arreglar los nombres de los campos
    $crud->display_as('nombre_especialidad', 'Especilidad del médico');
    
    // Configurar las columnas que queremos que se muestren en la lista
    $crud->columns('nombre_especialidad');
    // $crud->edit_fields('descrip_carrera');

    // Orden de la lista
    $crud->order_by('nombre_especialidad','ASC'); //revisar esta linea

    // Validaciones
    $crud->set_rules('nombre_especialidad', 'Especialidad',"required|alpha_space|max_length[50]|min_length[2]");

    // // Renderiza la Vista
    $salida = $crud->render();
    
    // // Llama a la función que va a mostrar la Vista
    // $this->salida($output);
      
    $this->load->view('header_admin',$salida);
    $this->load->view('especialidad',$salida);
    $this->load->view('footer');
    
	}

}


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
          redirect('/');
        }
        
        $this->load->library('form_validation');
        $this->load->library('datemanager');


        $this->load->model("especialidades");
        $this->load->model("mcitas");
        $this->load->model("mconsultas");
        $this->load->model("mmedicos");

    }
	
    public function index($data = null)
    {
      
        $this->load->view('header_admin');
        $this->load->view("busqueda",$data);
        $this->load->view('footer');
        
    }

    public function printR($data = null, $vista = "busqueda")
    {
      
      // echo "entra";
        $this->load->view('header_rp');
        $this->load->view($vista,$data);
        
    }


// rced_b
    public function rced_b($data = null)
    {
      
        $this->index(array('rp' => "rced"));
        
    }

    
// rce_b
    public function rce_b($data = null)
    {

        $this->index(array('rp' => 'rce', 'medicos' => $this->mmedicos->get()));
        
    }

// rceg_b
    public function rceg_b($data = null)
    {
      
        $this->index(array('rp' => "rceg"));
        
    }

	public function rceg()
    {
        try {

            $this->form_validation->set_rules('desde', 'Fecha de Comienzo', 'required');
            $this->form_validation->set_rules('hasta', 'Fecha hasta', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $this->index(array("rp" => $this->input->post("rp")));
            }
            else
            {

                $data['desde'] = $this->input->post("desde");
                $data['hasta'] = $this->input->post("hasta");
                $data['especialidades'] = $this->especialidades->get();
                $this->printR($data,"rceg_rp");

            }
        
        } 
        catch(Exception $e) 
        {

            show_error($e->getMessage().' --- '.$e->getTraceAsString());

        }

    }
    
    public function rced()
    {
        try {

            $this->form_validation->set_rules('desde', 'Fecha de Comienzo', 'required');
            $this->form_validation->set_rules('hasta', 'Fecha hasta', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $this->index(array("rp" => $this->input->post("rp")));
            }
            else
            {

                $data['desde'] = $this->input->post("desde");
                $data['hasta'] = $this->input->post("hasta");
                $data['especialidades'] = $this->especialidades->get();
                $this->printR($data,"rced_rp");

            }
        
        } 
        catch(Exception $e) 
        {

            show_error($e->getMessage().' --- '.$e->getTraceAsString());

        }

    }
    
    public function rce()
    {
        try {

            $this->form_validation->set_rules('desde', 'Fecha de Comienzo', 'required');
            $this->form_validation->set_rules('hasta', 'Fecha hasta', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                $this->index(array("rp" => $this->input->post("rp")));
            }
            else
            {

                $data['desde'] = $this->input->post("desde");
                $data['hasta'] = $this->input->post("hasta");
                $data['id_medico'] = $this->input->post("id_medico");
                $data['especialidades'] = $this->especialidades->get();
                $this->printR($data,"rce_rp");

            }
        
        } 
        catch(Exception $e) 
        {

            show_error($e->getMessage().' --- '.$e->getTraceAsString());

        }

    }

}


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
  
  function __construct()
	{
		
    parent::__construct();

    $this->load->library('session');
    if (!$this->session->userdata("logged_in")){
      redirect('/');
    }   
    $this->load->library("form_validation");

  }
	
	public function index($data = null)
	{
  
    $this->load->view('header_admin');
    $this->load->view('admin',$data);
    $this->load->view('footer');
    
	}

}


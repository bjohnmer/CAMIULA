<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Citas extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
          redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }
	
	public function index($data = null)
	{

        $crud = new grocery_CRUD();

        // Cnfigurar tabla de trabajo
        $crud->set_table('tbl_citas');

        // // Configurar el sujeto
        $crud->set_subject('Cita');

        // Relaciones con las tablas Paciente y Médico
        $crud->set_relation("id_medico","tbl_medicos","{codigo_medico} | {nombre_medico} {apellido_medico}");
        $crud->set_relation("id_paciente","tbl_pacientes","{historia_paciente} - {cedula_paciente} - {nombre_paciente} {apellido_paciente}");

        // Quitar el Id de la tabla
        $crud->unset_fields("id_cita","fechaCreacion_cita","fechaActualizacion_cita");

        // Arreglar los nombres de los campos
        $crud->display_as('fechaProgramada_cita', 'Fecha de Programación');
        $crud->display_as('id_paciente', 'Paciente');
        $crud->display_as('id_medico', 'Médico');

        // Configurar las columnas que queremos que se muestren en la lista
        $crud->columns('fechaProgramada_cita','id_medico','id_paciente');
        // $crud->edit_fields('descrip_carrera');

        // Orden de la lista
        $crud->order_by('fechaProgramada_cita','DESC');

        // Validaciones
        $crud->set_rules('fechaProgramada_cita', 'Fecha de Programación',"required");
        $crud->set_rules('id_medico', 'Médico',"required");
        $crud->set_rules('id_paciente', 'Paciente',"required");
    
        // Renderiza la Vista
        $salida = $crud->render();
        
        $this->load->view('header_admin',$salida);
        $this->load->view('citas',$salida);
        $this->load->view('footer');
    
	}

}
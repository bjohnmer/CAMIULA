<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consultas extends CI_Controller {
  
    function __construct()
	{	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
          redirect('/');
        }
        $this->load->library('grocery_CRUD');
    }
	
	public function index($data = null)
	{

        $crud = new grocery_CRUD();

        // Configurar tabla de trabajo
        $crud->set_table('tbl_consultas');

        // $crud->unset_fields('id_consulta');
        // Inhabilitar el Editor de Texto enrriquecido para los textareas
        $crud->unset_texteditor("diagnostico_consulta");

        // Configurar el sujeto
        $crud->set_subject('Consulta');

        // Relaciones con las tablas Paciente y Médico
        // $crud->set_relation("id_paciente","tbl_pacientes","historia_paciente");
        $crud->set_relation("id_medico","tbl_medicos","{codigo_medico} | {nombre_medico} {apellido_medico}");
        $crud->set_relation("id_paciente","tbl_pacientes","{historia_paciente} - {nombre_paciente} {apellido_paciente}");

        // Quitar el Id de la tabla
        $crud->unset_fields("id_consulta","fechaActualizacion_consulta");

        // Arreglar los nombres de los campos
        $crud->display_as('tipo_consulta', 'Tipo de Consulta');
        $crud->display_as('id_medico', 'Médico');
        $crud->display_as('id_paciente', 'Paciente');
        $crud->display_as('fechaCreacion_consulta', 'Fecha de Consulta');
        $crud->display_as('primera_consulta', 'Primera Consulta');
        $crud->display_as('diagnostico_consulta', 'Diagnostico');
        $crud->display_as('recipe_consulta', 'Recipe');
            
        // Configurar las columnas que queremos que se muestren en la lista
        $crud->columns('fechaCreacion_consulta','tipo_consulta','primera_consulta','diagnostico_consulta','recipe_consulta');
        // $crud->edit_fields('descrip_carrera');

        // Orden de la lista
        $crud->order_by('fechaCreacion_consulta','DESC');
        
        // // Validaciones
        
        $crud->set_rules('fechaCreacion_consulta', 'Fecha de Consulta',"required");
        $crud->set_rules('tipo_consulta', 'Tipo de Consulta',"required");
        $crud->set_rules('primera_consulta', 'Primera Consulta',"required");
        $crud->set_rules('diagnostico_consulta', 'Diagnostico de la consulta',"required|min_length[5]");
        $crud->set_rules('recipe_consulta', 'Recipe',"required");

        // Renderiza la Vista
        $salida = $crud->render();
        
        $this->load->view('header_admin',$salida);
        $this->load->view('consultas',$salida);
        $this->load->view('footer');
        
	}

}


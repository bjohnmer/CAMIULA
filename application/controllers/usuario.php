<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }

    public function index($data = null)
    {

        $crud = new grocery_CRUD();

        // Cnfigurar tabla de trabajo
        $crud->set_table('tbl_usuario');

        // Configurar el sujeto
        $crud->set_subject('Usuario');

        // // Quitar el Id de la tabla
        $crud->unset_fields("fechaRegistro_usuario","fechaActualizacion_usuario");

        // Arreglar los nombres de los campos
        $crud->display_as('nombre_usaurio', 'Nombre');
        $crud->display_as('apellido_usaurio', 'Apellido');
        $crud->display_as('login_usuario', 'Login');
        $crud->display_as('pwd_usuario', 'Clave');
        $crud->display_as('cnf_usuario', 'Confirmación de Clave');


        // Configurar las columnas que queremos que se muestren en la lista
        $crud->columns("nombre_usuario","apellido_usuario","login_usuario");

        // Cambiar el tipo de campo de clave texto a password
        $crud->field_type('pwd_usuario', 'password');
        $crud->field_type('cnf_usuario', 'password');

        // Validaciones
        $crud->set_rules('nombre_usuario', 'Nombre del Usuario',"required|alpha_space|max_length[50]|min_length[2]");
        $crud->set_rules('apellido_usuario', 'Apellido del Usuario',"required|alpha_space|max_length[50]|min_length[2]");
        $crud->set_rules('login_usuario', 'Login',"required|alpha|max_length[20]|min_length[4]");
        $crud->set_rules('pwd_usuario', 'Clave',"max_length[20]|min_length[3]|matches[cnf_usuario]");
        $crud->set_rules('cnf_usuario', 'Confirmación de Clave',"max_length[20]|min_length[3]|matches[pwd_usuario]");

        // Encriptar la clave antes de guardar o actualizar
        $crud->callback_before_insert(array($this,'encrypt_pwd_callback'));
        $crud->callback_before_update(array($this,'encrypt_cnf_callback'));


        $crud->callback_edit_field('pwd_usuario',array($this,'vaciar_campo_pwd'));
        $crud->callback_edit_field('cnf_usuario',array($this,'vaciar_campo_cnf'));

        // Renderiza la Vista
        $salida = $crud->render();

        // Llama a la función que va a mostrar la Vista
        $this->load->view('header_admin',$salida);
        $this->load->view('usuarios',$salida);
        $this->load->view('footer');

    }

    function encrypt_pwd_callback($post_array, $primary_key = null)
    {
        $this->load->library('encrypt');
        $post_array['pwd_usuario'] = $this->encrypt->encode($post_array['pwd_usuario']);
        $post_array['cnf_usuario'] = $this->encrypt->encode($post_array['cnf_usuario']);
        
        $post_array['fechaRegistro_usuario'] = 'CURDATE()';
        return $post_array;
    }

    function encrypt_cnf_callback($post_array, $primary_key = null)
    {
        if (!empty($post_array['pwd_usuario']))
        {
            $post_array['pwd_usuario'] = md5($post_array['pwd_usuario']);
            $post_array['cnf_usuario'] = md5($post_array['cnf_usuario']);
        }
        else
        {
            unset($post_array['pwd_usuario']);
            unset($post_array['cnf_usuario']);
        }
        return $post_array;
    }

    function vaciar_campo_pwd($post_array, $primary_key = null)
    {
        return '<input type="password" maxlength="50" name="pwd_usuario" id="field-pwd_usuario">';
    }

    function vaciar_campo_cnf($post_array, $primary_key = null)
    {
        return '<input type="password" maxlength="50" name="cnf_usuario" id="field-pwd_usuario">';
    }

}


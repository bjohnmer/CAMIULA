<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medico extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }
	
    public function index($data = null)
    {

        $crud = new grocery_CRUD();

        // Cnfigurar tabla de trabajo
        $crud->set_table('tbl_medicos');


        // Relación con la tabla de especialidades
        $crud->set_relation('id_especialidad','tbl_especialidad','nombre_especialidad');

        // Inhabilitar el Editor de Texto enrriquecido para los textareas
        // $crud->unset_texteditor("direccion_paciente","direccionLaboral_paciente","direccionFamiliar_paciente");

        // // Configurar el sujeto
        $crud->set_subject('Medico');

        // // Quitar el Id de la tabla
        $crud->unset_fields("id_medico","fechaCreacion_medico","fechaActualizacion_medico");

        // Arreglar los nombres de los campos
        $crud->display_as('nombre_medico', 'Nombre');
        $crud->display_as('apellido_medico', 'Apellido');
        $crud->display_as('id_especialidad', 'Especialidad');
        $crud->display_as('codigo_medico', 'Código');


        // Configurar las columnas que queremos que se muestren en la lista
        $crud->columns('nombre_medico', 'apellido_medico', 'id_especialidad');
        // $crud->edit_fields('descrip_carrera');


        $crud->callback_before_insert(array($this,'uppercase_codigo'));
        $crud->callback_before_update(array($this,'uppercase_codigo'));

        // Orden de la lista
        //$crud->order_by('historia_paciente','ASC');

        // Validaciones
        $crud->set_rules('especialidad_medico', 'Especialidad',"required");
        $crud->set_rules('nombre_medico', 'Nombre',"required|alpha_space|max_length[50]|min_length[2]");
        $crud->set_rules('apellido_medico', 'Apellido',"required|alpha_space|max_length[50]|min_length[2]");
        $crud->set_rules('codigo_medico', 'Código',"required|alpha_numeric|max_length[3]|min_length[3]");

        // Renderiza la Vista
        $salida = $crud->render();

        // Llama a la función que va a mostrar la Vista
        // $this->salida($output);
          
        $this->load->view('header_admin',$salida);
        $this->load->view('medicos',$salida);
        $this->load->view('footer');

    }


    function uppercase_codigo($post_array, $primary_key = null)
    {
        $post_array['codigo_medico'] = strtoupper($post_array['codigo_medico']);
        return $post_array;
    }


}


<p class="membrete">
  UNIVERSIDAD DE LOS ANDES <br>
  NÚCLEO UNIVERSITARIO "RAFAEL RANGEL" <br>
  CENTRO AMBULATORIO MÉDICO INTEGRAL U.L.A. <br>
  C.A.M.I.U.L.A. <br>
  TRUJILLO, ESTADO TRUJILLO.
  <span class="titulor">Clasificación Económica</span>
  <span class="parametros">Desde: <?=$desde?> - Hasta: <?=$hasta?></span>
  <br>
  <span class="titulor">Médico: <?php $medico = $this->mmedicos->get(array('id_medico'=>$id_medico)); echo $medico[0]->nombre_medico." ".$medico[0]->apellido_medico;?></span>
  
</p>
<table width="70%" align="center" class="datos">
  <thead>
    <tr>
      <td rowspan="2"><strong>Fecha</strong></td>
      <td colspan="10" align="center"><strong>Información Estadística por Clasificación Económica</strong></td>
    </tr>
    <tr>
      <td><strong>Cortesías</strong></td>
      <td><strong>Trabajador</strong></td>
      <td><strong>Profesor</strong></td>
      <td><strong>Estudiante</strong></td>
      <td><strong>Obrero</strong></td>
      <td><strong>Familiar Trabajador</strong></td>
      <td><strong>Familiar Profesor</strong></td>
      <td><strong>Familiar Estudiante</strong></td>
      <td><strong>Familiar Obrero</strong></td>
      <td><strong>TOTALES</strong></td>
    </tr>
  </thead>
  <tbody>
    <?php 
      $fechas = $this->mconsultas->getDates(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta >=' => $this->datemanager->date2mySQL($desde), 'fechaCreacion_consulta <=' => $this->datemanager->date2mySQL($hasta))); 
    ?>
    <?php foreach ($fechas as $fecha): ?>
      <tr>
        <td><?=$f = $fecha->fechaCreacion_consulta?></td>
        <td><?=$cortesias = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tipo_consulta' => 'Cortesía'))?></td>
        <td><?=$trabajadores = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Trabajador'))?></td>
        <td><?=$profesores = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Profesor'))?></td>
        <td><?=$estudiantes = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Estudiante'))?></td>
        <td><?=$obreros = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Obrero'))?></td>
        <td><?=$ftrabajadores = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Familiar Trabajador'))?></td>
        <td><?=$fprofesor = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Familiar Profesor'))?></td>
        <td><?=$festudiante = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Familiar Estudiante'))?></td>
        <td><?=$fobrero = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $id_medico, 'fechaCreacion_consulta' => $f, 'tbl_pacientes.claseEconomica_paciente' => 'Familiar Obrero'))?></td>
        <td><?=$cortesias + $trabajadores + $profesores + $estudiantes + $obreros + $ftrabajadores + $fprofesor + $festudiante + $fobrero?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
<span class="parametros">Fuente: Morbilidad de Consulta Externa - Historias Médicas - CAMIULA</span>

<p class="membrete">
  UNIVERSIDAD DE LOS ANDES <br>
  NÚCLEO UNIVERSITARIO "RAFAEL RANGEL" <br>
  CENTRO AMBULATORIO MÉDICO INTEGRAL U.L.A. <br>
  C.A.M.I.U.L.A. <br>
  TRUJILLO, ESTADO TRUJILLO.
  <span class="titulor">Consultas Realizadas por Especialidad</span>
  <span class="parametros">Desde: <?=$desde?> - Hasta: <?=$hasta?></span>
</p>
<table width="70%" align="center" class="datos">
  <thead>
    <tr>
      <td rowspan="2"><strong>Médicos</strong></td>
      <td colspan="9" align="center"><strong>Información Estadística de Consultas</strong></td>
    </tr>
    <tr>
      <td><strong>Programadas<br>(Citadas)</strong></td>
      <td><strong>Ejecutadas<br>(Atendidas)</strong></td>
      <td><strong>Emergencias</strong></td>
      <td><strong>Primeras<br>Consultas</strong></td>
      <td><strong>Suscesivas</strong></td>
      <td><strong>Inasistencias</strong></td>
      <td><strong>%<br>Inasistencias</strong></td>
      <td><strong>Cambio<br>de Cita</strong></td>
      <td><strong>Recipes</strong></td>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($especialidades)): ?>

      <?php $TGPro = 0; $TGEje = 0; $TGEme = 0; $TGPri = 0; $TGSus = 0; $TGIna = 0; $TGPIna = 0; $TGCcit = 0; $TGReci = 0; ?>
      <?php foreach ($especialidades as $especialidad): ?>
      <?php $TPro = 0; $Teje = 0; $Teme = 0; $Tpri = 0; $Tsus = 0; $Tina = 0; $Tpina = 0; $Tccit = 0; $Treci = 0; ?>
          <tr>
            <td colspan="11"><h4><?=$especialidad->nombre_especialidad?></h4></td>
          </tr>        
        <?php $num = $this->mmedicos->getNumber(array("tbl_medicos.id_especialidad" => $especialidad->id_especialidad));?>
        <?php 
          $medicos = $this->mmedicos->get(array("tbl_medicos.id_especialidad" => $especialidad->id_especialidad));
        ?>
        <?php foreach ($medicos as $medico): ?>
        <tr>
          <td><?=$medico->nombre_medico?> <?=$medico->apellido_medico?></td>
          <td><?=$programadas = $this->mcitas->getNumber(array('tbl_citas.id_medico' => $medico->id_medico,'id_especialidad' => $especialidad->id_especialidad, "fechaProgramada_cita >=" => $this->datemanager->date2mySQL($desde), "fechaProgramada_cita <=" => $this->datemanager->date2mySQL($hasta)));?><?php $TPro += $programadas; ?></td>
          <td><?=$ejecutadas = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $medico->id_medico, 'id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta)));?><?php $Teje += $ejecutadas; ?></td>
          <td><?=$emergencias = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $medico->id_medico, 'id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "tipo_consulta" => "Emergencia"));?><?php $Teme += $emergencias; ?></td>
          <td><?=$primera = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $medico->id_medico, 'id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "primera_consulta" => "Si"));?><?php $Tpri += $primera; ?></td>
          <td><?=$suscesivas = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $medico->id_medico, 'id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "tipo_consulta" => "Citada"));?><?php $Tsus += $suscesivas; ?></td>
          <td><?=(!empty($programadas)) ? $inasistencias = $programadas - $suscesivas - $emergencias : 0?> <?php (!empty($programadas)) ? $Tina += $inasistencias : 0?></td>
          <td><?=(!empty($inasistencias) && !empty($programadas)) ? $porcien_inasistencias = $inasistencias*100/$programadas : 0?> <?php (!empty($programadas)) ? $Tpina += $porcien_inasistencias : 0?></td>
          <td><?=$cambio_cita = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $medico->id_medico, 'id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "tipo_consulta" => "Cambio de Cita"));?> <?php $Tccit += $cambio_cita?></td>
          <td><?=$recipes = $this->mconsultas->getNumber(array('tbl_consultas.id_medico' => $medico->id_medico, 'id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "recipe_consulta" => "Si"));?> <?php $Treci += $recipes?></td>
        </tr>
        <?php endforeach ?>
        <tr>
          <td><strong>TOTAL</strong></td>
          <td><?=$TPro?><?php $TGPro += $TPro;?></td>
          <td><?=$Teje?><?php $TGEje += $Teje;?></td>
          <td><?=$Teme?><?php $TGEme += $Teme;?></td>
          <td><?=$Tpri?><?php $TGPri += $Tpri;?></td>
          <td><?=$Tsus?><?php $TGSus += $Tsus;?></td>
          <td><?=$Tina?><?php $TGIna += $Tina;?></td>
          <td><?=$Tpina?><?php $TGPIna += $Tpina;?></td>
          <td><?=$Tccit?><?php $TGCcit += $Tccit;?></td>
          <td><?=$Treci?><?php $TGReci += $Treci;?></td>
        </tr>
      <?php endforeach ?>
        <tr>
          <td colspan="11">&nbsp;</td>
        </tr>
        <tr>
          <td><strong>TOTAL GENERAL</strong></td>
          <td><?=$TGPro?></td>
          <td><?=$TGEje?></td>
          <td><?=$TGEme?></td>
          <td><?=$TGPri?></td>
          <td><?=$TGSus?></td>
          <td><?=$TGIna?></td>
          <td><?=$TGPIna?></td>
          <td><?=$TGCcit?></td>
          <td><?=$TGReci?></td>
        </tr>
    <?php endif ?>
  </tbody>
</table>
<span class="parametros">Fuente: Morbilidad de Consulta Externa - Historias Médicas - CAMIULA</span>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title> CAMIULA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=base_url()?>css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url()?>css/personal.css" rel="stylesheet">
    <link href="<?=base_url()?>css/bootstrap-responsive.css" rel="stylesheet">
  

    <?php if (!empty($css_files)): ?>
      <?php foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
      <?php endforeach; ?>
    <?php endif ?>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <link href="<?=base_url()?>css/overcast/jquery-ui-1.10.3.custom.css" rel="stylesheet">
    
    <!-- Fav and touch icons -->
    

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">

  </head>

  <body>

    <div class="container">

      <div class="masthead">
        <ul class="nav nav-pills pull-right">
          <li class="active"><a href="<?=base_url()?>admin">Inicio</a></li>
          
          <li><a href="<?=base_url()?>consultas">Consultas</a></li>
          <li><a href="<?=base_url()?>citas">Citas</a></li>
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Reportes <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?=base_url()?>reportes/rce_b">Por Clasificación Económica</a></li>
              <li class="divider"></li>
              <li><a href="<?=base_url()?>reportes/rceg_b">Consultas por Especialidad (General)</a></li>
              <li><a href="<?=base_url()?>reportes/rced_b">Consultas por Especialidad (Detallado)</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Datos Iniciales <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?=base_url()?>especialidad">Especialidades</a></li>
              <li><a href="<?=base_url()?>medico">Médicos</a></li>
              <li class="divider"></li>
              <li><a href="<?=base_url()?>paciente">Pacientes</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Configuración <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?=base_url()?>usuario">Usuarios</a></li>
            </ul>
          </li>
        </ul>
        <h3 class="muted"> CAMIULA </h3>
      </div>

      <hr>

      <?php if ($this->session->userdata("logged_in")){ ?>
      <div class="row">
        <!-- <div class="span6"></div> -->
        <div class="span12">
          <p class="pull-right">
            <?=$this->session->userdata("nombre_usuario")." ".$this->session->userdata("apellido_usuario")." | ".$this->session->userdata("login_usuario")?>
            <a href="<?=base_url()?>login/logout" class="btn btn-danger">Salir <i class="icon-off icon-white"></i></a>
          </p> 
        </div>
      </div>
      <?php } ?>

      <hr>
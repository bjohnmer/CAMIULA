<h1>Acerca de </h1>
<p>
El Centro de Atención Médica Integral de la Universidad de Los Andes (CAMIULA), nace en el año 1.958 bajo la figura de la Oficina de Bienestar Estudiantil (O.B.E) siendo para ese momento Rector el Dr. Pedro Rincón Gutiérrez quien designa al Dr. Ciro Maldonado como su primer director, su función principal era la de brindar asistencia médica a los estudiantes de la Universidad de Los Andes, ésta constituyó la primera iniciativa de crear un organismo de tipo asistencial dentro de la Universidad de los Andes, lo cual puede considerarse como la etapa inicial de gestación del CAMIULA. Posteriormente, en los años sesenta, se incluyen como beneficiarios a los obreros y empleados de la Universidad gracias a las reivindicaciones obtenidas en los contratos colectivos.</p> 

  <p>Esta comunidad tuvo dos momentos, considerándose de alto nivel educativo donde casi todo su personal es de nivel profesional universitario, donde la educación y superación permanente son estímulo a través de una política de participación integral y de una gerencia de desarrollo organizacional, y además de los problemas de salud se ventilan también los problemas de política universitaria nacional que a través del tiempo ha creado un ambiente propicio como semillero de nuevos líderes a todos los niveles y cada uno de los cinco sindicatos afiliados al Centro Ambulatorio Médico Odontológico de la Universidad de los Andes (CAMOULA), cuyo nombre la identificaba en un principio. En 1.995 el Centro Ambulatorio Medico odontológico de la Universidad de los Andes (CAMOULA), pasa a ser el Centro de Atención Medica Integral de la Universidad de los Andes (CAMIULA,) debido a la aplicación del nuevo reglamento interno de la Universidad de los Andes hasta el presente.</p>

  <p>El Centro Ambulatorio Medico Integral de la Universidad de los Andes cuenta con un espacio físico de dos plantas, la planta baja dispone de tres consultorios (odontología, medicina general y ginecología), un área dividida en sala de cura y sala de observación y el departamento de registros médicos; en la parte alta cuenta con cuatro consultorios (cirugía general, pediatría, reumatología y nutrición),  y el departamento de coordinación CAMIULA. También dispone 6 baños, cuatro internos para los consultorios y dos públicos; aéreas verdes a su alrededor y un estacionamiento para el personal que labora allí.</p>
  
  <p>Además labora en dos turnos, en la mañana de 7am a 1pm y en la tarde de 1pm a 7pm, contando para sus labores, con personal para cada turno, comprende siete médicos especiales, un odontólogo, un nutricionista, y un medico general como personal medico; para las demás labores  y servicios dispone de tres enfermeros, personal de mantenimiento, auxiliar de farmacia, oficina y de registro médicos y estadísticas.</p>

<h3>Filosofía de gestión</h3>

<h4>Objetivos:</h4>

<ul>
  <li>Fomentar y preservar la salud de la comunidad universitaria, con el fin de mejorar su calidad de vida en el ámbito laboral, familiar y comunitario.</li>
  <li>Desarrollar programas de prevención par orientar a la familia y a las comunidades de la región, garantizando una mejor calidad de vida.
</li>
  <li>Satisfacer las necesidades básicas de diagnostico y tratamientos de patología existentes.
</li>
  <li>Apoyar las actividades de docencia en las áreas de salud de la Universidad de los Andes.
</li>
  <li>Fomentar las actividades de extensión.</li>
</ul>

  <h3>Misión</h3>
  <p>La misión de CAMIULA es la de brindar servicios de salud integral, de calidad, oportunos y confiables a la comunidad universitaria, al menor costo en los tres niveles de atención medica, en el plano preventivo, de rehabilitación y restitución de salud.</p>

  <h3>Visión</h3>
  <p>Desarrollar una clínica integral de prestigio nacional, tomando en cuanta las ventajas competitivas del mercado, y cuya fortalezas internas se basan en la integración del grupo de trabajo, y de la preparación constante por parte de los trabajadores de la institución, a través del mejoramiento académico que le permitan desarrollar una formación idónea, a fin de garantizar una formación en salud de alta calidad.</p>

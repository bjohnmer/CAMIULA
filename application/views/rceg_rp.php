<p class="membrete">
  UNIVERSIDAD DE LOS ANDES <br>
  NÚCLEO UNIVERSITARIO "RAFAEL RANGEL" <br>
  CENTRO AMBULATORIO MÉDICO INTEGRAL U.L.A. <br>
  C.A.M.I.U.L.A. <br>
  TRUJILLO, ESTADO TRUJILLO.
  <span class="titulor">Consultas Realizadas por Especialidad</span>
  <span class="parametros">Desde: <?=$desde?> - Hasta: <?=$hasta?></span>
</p>
<table width="70%" align="center" class="datos">
  <thead>
    <tr>
      <td rowspan="2"><strong>Orden</strong></td>
      <td rowspan="2"><strong>Especialidad</strong></td>
      <td colspan="10" align="center"><strong>Información Estadística de Consultas</strong></td>
    </tr>
    <tr>
      <td><strong>Programadas<br>(Citadas)</strong></td>
      <td><strong>Ejecutadas<br>(Atendidas)</strong></td>
      <td><strong>Emergencias</strong></td>
      <td><strong>Primeras<br>Consultas</strong></td>
      <td><strong>Suscesivas</strong></td>
      <td><strong>Inasistencias</strong></td>
      <td><strong>%<br>Inasistencias</strong></td>
      <td><strong>Cambio<br>de Cita</strong></td>
      <td><strong>Recipes</strong></td>
    </tr>
  </thead>
  <tbody>
    <?php if (!empty($especialidades)): ?>
      <?php $i = 0; ?>
      <?php foreach ($especialidades as $especialidad): ?>
        <tr>
          <td><?=$i?></td>
          <td><?=$especialidad->nombre_especialidad?></td>
          <td><?=$programadas = $this->mcitas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaProgramada_cita >=" => $this->datemanager->date2mySQL($desde), "fechaProgramada_cita <=" => $this->datemanager->date2mySQL($hasta)));?></td>
          <td><?=$this->mconsultas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta)));?></td>
          <td><?=$emergencias = $this->mconsultas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "tipo_consulta" => "Emergencia"));?></td>
          <td><?=$this->mconsultas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "primera_consulta" => "Si"));?></td>
          <td><?=$suscesivas = $this->mconsultas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "tipo_consulta" => "Citada"));?></td>
          <td><?=(!empty($programadas)) ? $inasistencias = $programadas - $suscesivas - $emergencias : 0?></td>
          <td><?=(!empty($inasistencias) && !empty($programadas)) ? $porcien_inasistencias = $inasistencias*100/$programadas : 0?></td>
          <td><?=$cambio_cita = $this->mconsultas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "tipo_consulta" => "Cambio de Cita"));?></td>
          <td><?=$recipes = $this->mconsultas->getNumber(array('id_especialidad' => $especialidad->id_especialidad, "fechaCreacion_consulta >=" => $this->datemanager->date2mySQL($desde), "fechaCreacion_consulta <=" => $this->datemanager->date2mySQL($hasta), "recipe_consulta" => "Si"));?></td>
        </tr>
      <?php endforeach ?>
    <?php endif ?>
  </tbody>
</table>
<span class="parametros">Fuente: Morbilidad de Consulta Externa - Historias Médicas - CAMIULA</span>

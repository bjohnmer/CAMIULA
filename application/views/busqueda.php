

      <div class="jumbotron">
        <p>
          <h3>Indique el rango de fechas a buscar</h3>
          <?php if (!empty($rp)): ?>
          <form action="<?=base_url()?>reportes/<?=$rp?>" method="post" class="bs-docs-example form-inline">
            <p>
              <input class="input" id="desde" name="desde" placeholder="<?=date("d/m/Y")?>" type="text">
              <input class="input" id="hasta" name="hasta" placeholder="<?=date("d/m/Y")?>" type="text">
              <?php if (!empty($rp)): ?>
                <?php if ($rp == "rce"): ?>
                  <?php if (!empty($medicos)): ?>
                  <select name="id_medico" id="id_medico">
                    <?php foreach ($medicos as $medico): ?>
                      <option value="<?=$medico->id_medico?>"><?=$medico->codigo_medico?> | <?=$medico->nombre_medico?> <?=$medico->apellido_medico?></option>
                    <?php endforeach ?>
                  </select>
                  <?php endif ?>
                <?php endif ?>
              <?php endif ?>
              <input type="hidden" name="rp" value="<?=$rp?>">
            </p>
            <p>
              <button type="submit" class="btn btn-success">Buscar</button>
            </p>
            <p>

            <?php if (!empty($mensaje)): ?>
            <div id="report-error" class="alert alert-error" style="display:block;">
              <h5>Se han encontrado errores</h5>
                <?=$mensaje?>
            </div>
            <?php endif ?>

              <?php if (validation_errors()): ?>
              <div id="report-error" class="alert alert-error" style="display:block;">
                <h5>Se han encontrado errores</h5>
                <?=validation_errors()?>
              </div>
              <?php endif ?>
            </p>
              

          </form>
          <?php endif ?>
        </p>

      </div>

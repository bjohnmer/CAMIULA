
      <hr>

      <div class="row-fluid marketing">
        <div class="span12">
          <h4>Desarrollado por:</h4>
          <p> Castellanos Yanderi -<a href="mailto:yanderi01@hotmail.com">yanderi01@hotmail.com</a></p>
          <h4>Asesor de Programación y Desarrollo:</h4>
          <p> Johnmer Bencomo -<a href="mailto:bjohnmer@gmail.com">bjohnmer@gmail.com</a></p>
      </div>

       
      </div>

      <hr>

      <div class="footer">
        <p>&copy; Todos los derechos reservados - CAMIULA <?=date("Y")?> </p>
      </div>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>js/bootstrap.js"></script>
    <script src="<?=base_url()?>js/jquery.js"></script>
    <script src="<?=base_url()?>js/bootstrap-dropdown.js"></script>
    
    <!--
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tab.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-button.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
-->

  <?php if (!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
  <?php endif ?>


  <?php if (empty($js_files)): ?>
    <script src="<?=base_url()?>js/jquery-1.9.1.js"></script>
  <?php endif ?>
  <script src="<?=base_url()?>js/jquery-ui-1.10.3.custom.js"></script>

  <script>
    $(document).ready(function(){
      
      $('#desde, #hasta').datepicker({
          dateFormat: "dd/mm/yy",
          showButtonPanel: true,
          changeMonth: true,
          yearRange: "-80:",
          changeYear: true,
          maxDate:0
      });

      
    });
  </script>

  </body>
</html>

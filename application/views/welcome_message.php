

      <div class="jumbotron">
        <h2>Aplicación Web para el registro y control de consultas CAMIULA</h2>


        <p>
          <h3>Ingrese al sistema</h3>
          <form action="<?=base_url()?>login/entrar" method="post" class="bs-docs-example form-inline">
            <p>
              <input class="input" name="login_usuario" placeholder="Usuario" type="text">
              <input class="input" name="pwd_usuario" placeholder="Clave" type="password">
            </p>
            <p>
              <button type="submit" class="btn btn-success">Ingresar</button>
            </p>
            <p>

            <?php if (!empty($mensaje)): ?>
            <div id="report-error" class="alert alert-error" style="display:block;">
              <h5>Se han encontrado errores</h5>
                <?php echo $mensaje; ?>
            </div>
            <?php endif ?>

              <?php if (validation_errors()): ?>
              <div id="report-error" class="alert alert-error" style="display:block;">
                <h5>Se han encontrado errores</h5>
                <?=validation_errors()?>
              </div>
              <?php endif ?>
            </p>
              

          </form>
        </p>

      </div>

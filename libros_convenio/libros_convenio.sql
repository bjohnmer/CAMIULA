-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 19, 2013 at 06:28 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `libros_convenio`
--

-- --------------------------------------------------------

--
-- Table structure for table `libros_autores`
--

CREATE TABLE IF NOT EXISTS `libros_autores` (
  `id_libros_autor` int(11) NOT NULL AUTO_INCREMENT,
  `id_libro` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  PRIMARY KEY (`id_libros_autor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alumnos`
--

CREATE TABLE IF NOT EXISTS `tbl_alumnos` (
  `id_alumno` int(11) NOT NULL AUTO_INCREMENT,
  `cedula_alumno` int(11) NOT NULL,
  `nombres_alumno` varchar(100) NOT NULL,
  `telf_alumno` varchar(50) NOT NULL,
  `email_alumno` varchar(50) NOT NULL,
  PRIMARY KEY (`id_alumno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_alumnos`
--

INSERT INTO `tbl_alumnos` (`id_alumno`, `cedula_alumno`, `nombres_alumno`, `telf_alumno`, `email_alumno`) VALUES
(1, 0, 'Lizmar Delgado', '', ''),
(2, 0, 'Lizmar SÃ¡las', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_autores`
--

CREATE TABLE IF NOT EXISTS `tbl_autores` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `nombres_autor` varchar(100) NOT NULL,
  PRIMARY KEY (`id_autor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_autores`
--

INSERT INTO `tbl_autores` (`id_autor`, `nombres_autor`) VALUES
(1, 'Lizmar Delgado'),
(2, 'Lismar SÃ¡las'),
(3, 'Jean LeÃ³n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_libros`
--

CREATE TABLE IF NOT EXISTS `tbl_libros` (
  `id_libro` int(11) NOT NULL AUTO_INCREMENT,
  `cota_libro` varchar(10) NOT NULL,
  `titulo_libro` varchar(100) NOT NULL,
  `editorial_libro` varchar(100) NOT NULL,
  `anio_libro` int(11) NOT NULL,
  `descripcion_libro` text NOT NULL,
  PRIMARY KEY (`id_libro`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usuarios`
--

CREATE TABLE IF NOT EXISTS `tbl_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `login_usuario` varchar(50) NOT NULL,
  `clave_usuario` varchar(50) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `login_usuario`, `clave_usuario`) VALUES
(1, 'admin', '202cb962ac59075b964b07152d234b70'),
(2, 'lizmar', '81dc9bdb52d04dc20036dbd8313ed055');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

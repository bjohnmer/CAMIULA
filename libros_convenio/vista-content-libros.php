<div id="tabla-datos">
	<div id="caja-boton">
		<a href="?p=libros&f=nuevo-libro" class="boton">Nuevo</a>
		&nbsp;&nbsp;
		<a href="#" onclick="window.print();" class="boton">Imprimir</a>
	</div>
	<table>
	
		<tr>
			<td>COTA</td>
			<td>TÍTULO</td>
			<td>EDITORIAL</td>
			<td>AÑO</td>
			<td>MODIFICAR</td>
			<td>ELIMINAR</td>
		</tr>
		<?php
			$libros = showALL("titulo_libro");
			foreach ($libros as $key => $value) {
				echo "<tr>";

				echo "<td>".$value['cota_libro']."</td>";
				echo "<td>".$value['titulo_libro']."</td>";
				echo "<td>".$value['editorial_libro']."</td>";
				echo "<td>".$value['anio_libro']."</td>";

				echo "<td align='center'><a href='?p=libros&f=modificar-libro&id=".$value['id_libro']."'>M</a></td>";
				echo "<td align='center'><a onclick='return mensajeEliminar();' href='?p=libros&f=eliminar-libro&id=".$value['id_libro']."'>X</a></td>";
				echo "</tr>";
			}
		?>
	</table>
</div>
<?php 
	
	$alumno = getById($_GET['id']);

?>
<div id="tabla-datos">
	
	<h3>MODIFICAR ALUMNO</h3>

	<form action="?p=alumnos&f=editar-alumno" method="post">
		
    <label for="cedula_alumno">Cédula:</label>
    <input type="text" name="cedula_alumno" id="cedula_alumno" value="<?php echo $alumno[0]['cedula_alumno']?>">
    <label for="nombres_alumno">Nombre Completo:</label>
    <input type="text" name="nombres_alumno" id="nombres_alumno" value="<?php echo $alumno[0]['nombres_alumno']?>">
    <label for="telf_alumno">Teléfonos</label>
    <input type="text" name="telf_alumno" id="telf_alumno" value="<?php echo $alumno[0]['telf_alumno']?>">
    <label for="email_alumno">Correo Electrónico:</label>
    <input type="text" name="email_alumno" id="email_alumno" value="<?php echo $alumno[0]['email_alumno']?>">
		<input type="hidden" name="id_alumno" value="<?php echo $alumno[0]['id_alumno']; ?>">
		<button class="boton">Modificar</button>
		<input type="reset" class="boton" value="Limpiar">
    
	</form>
	
</div>
<div id="tabla-datos">
	<div id="caja-boton">
		<a href="?p=autores&f=nuevo-autor" class="boton">Nuevo</a>
		&nbsp;&nbsp;
		<a href="#" onclick="window.print();" class="boton">Imprimir</a>
	</div>
	<table>
		<tr>
			<td>ID</td>
			<td>NOMBRE</td>
			<td>MODIFICAR</td>
			<td>ELIMINAR</td>
		</tr>
		<?php
			$autores = showALL("nombres_autor");
			foreach ($autores as $key => $value) {
				echo "<tr>";
				echo "<td>".$value['id_autor']."</td>";
				echo "<td>".$value['nombres_autor']."</td>";
				echo "<td align='center'><a href='?p=autores&f=modificar-autor&id=".$value['id_autor']."'>M</a></td>";
				echo "<td align='center'><a onclick='return mensajeEliminar();' href='?p=autores&f=eliminar-autor&id=".$value['id_autor']."'>X</a></td>";
				echo "</tr>";
			}
		?>
	</table>
</div>
<?php 
	
	$libro = getById($_GET['id']);

?>
<div id="tabla-datos">
	
	<h3>MODIFICAR libro</h3>

	<form action="?p=libros&f=editar-libro" method="post">
		

		<label for="cota_libro">Cota:</label>
    <input type="text" name="cota_libro" id="cota_libro" value="<?php echo $libro[0]['cota_libro']; ?>">
    <label for="titulo_libro">Título</label>
    <input type="text" name="titulo_libro" id="titulo_libro" value="<?php echo $libro[0]['titulo_libro']; ?>">
    <label for="editorial_libro">Editorial</label>
    <input type="text" name="editorial_libro" id="editorial_libro" value="<?php echo $libro[0]['editorial_libro']; ?>">
    <label for="anio_libro">Año</label>
    <input type="text" name="anio_libro" id="anio_libro" value="<?php echo $libro[0]['anio_libro']; ?>">
    <label for="descripcion_libro">Descripción</label>
    <textarea name="descripcion_libro" id="descripcion_libro" cols="30" rows="10" ><?php echo $libro[0]['descripcion_libro'];  ?></textarea>
		<input type="hidden" name="id_libro" value="<?php echo $libro[0]['id_libro']; ?>">
		<button class="boton">Modificar</button>
		<input type="reset" class="boton" value="Limpiar">
	</form>
	
</div>
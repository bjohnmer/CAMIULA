<?php 
	
	$autor = getById($_GET['id']);

?>
<div id="tabla-datos">
	
	<h3>MODIFICAR AUTOR</h3>

	<form action="?p=autores&f=editar-autor" method="post">
		<label for="nombres-autor">Nombre</label>
		<input type="text" name="nombres_autor" id="nombres_autor" placeholder="Nombre del Autor" value="<?php echo $autor[0]['nombres_autor'];  ?>">
		<input type="hidden" name="id_autor" value="<?php echo $autor[0]['id_autor']; ?>">
		<button class="boton">Modificar</button>
		<input type="reset" class="boton" value="Limpiar">
	</form>
	
</div>
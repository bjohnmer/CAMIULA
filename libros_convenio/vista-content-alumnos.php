<div id="tabla-datos">
	<div id="caja-boton">
		<a href="?p=alumnos&f=nuevo-alumno" class="boton">Nuevo</a>
		&nbsp;&nbsp;
		<a href="#" onclick="window.print();" class="boton">Imprimir</a>
	</div>
	<table>
	
		<tr>
			<td>CEDULA</td>
			<td>NOMBRE COMPLETO</td>
			<td>TELÉFONOS</td>
			<td>CORREO</td>
			<td>MODIFICAR</td>
			<td>ELIMINAR</td>
		</tr>
		<?php
			$alumnos = showALL("cedula_alumno");
			foreach ($alumnos as $key => $value) {
				echo "<tr>";

				echo "<td>".$value['cedula_alumno']."</td>";
				echo "<td>".$value['nombres_alumno']."</td>";
				echo "<td>".$value['telf_alumno']."</td>";
				echo "<td>".$value['email_alumno']."</td>";
				
				echo "<td align='center'><a href='?p=alumnos&f=modificar-alumno&id=".$value['id_alumno']."'>M</a></td>";
				echo "<td align='center'><a onclick='return mensajeEliminar();' href='?p=alumnos&f=eliminar-alumno&id=".$value['id_alumno']."'>X</a></td>";
				echo "</tr>";
			}
		?>
	</table>
</div>
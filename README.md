CAMIULA
=======

Aplicación Web para el registro y control de consultas de la Universidad de los Andes - Venezuela.

Asesoría: Diseño, Desarrollo e Ingeniería de Software Web

Herramientas utilizadas:

Front-end: HTML5, CSS3, Framework CSS Twitter Bootstrap (http://getbootstrap.com)

Back-end: PHP, Framework PHP Codeigniter (http://ellislab.com/codeigniter), Librería GroceryCRUD (http://www.grocerycrud.com/)

Base de Datos: MySQL



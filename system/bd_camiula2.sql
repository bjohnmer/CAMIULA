-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2013 at 10:47 AM
-- Server version: 5.5.31
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_camiula`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_citas`
--

CREATE TABLE IF NOT EXISTS `tbl_citas` (
  `id_cita` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `fechaCreacion_cita` datetime NOT NULL,
  `fechaProgramada_cita` date NOT NULL,
  `fechaActualizacion_cita` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cita`),
  KEY `id_paciente` (`id_paciente`,`id_medico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consultas`
--

CREATE TABLE IF NOT EXISTS `tbl_consultas` (
  `id_consulta` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `fechaCreacion_consulta` date NOT NULL,
  `tipo_consulta` enum('Emergencia','Cambio de Cita') NOT NULL,
  `primera_consulta` tinyint(1) NOT NULL,
  `diagnostico_consulta` text NOT NULL,
  `recipe_consulta` tinyint(1) NOT NULL,
  `fechaActualizacion_consulta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_consulta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_especialidad`
--

CREATE TABLE IF NOT EXISTS `tbl_especialidad` (
  `id_especialidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_especialidad` varchar(50) NOT NULL,
  `fechaCreacion_especialidad` datetime NOT NULL,
  `fechaActualizacion_especialidad` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_especialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_medicos`
--

CREATE TABLE IF NOT EXISTS `tbl_medicos` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_medico` varchar(50) NOT NULL,
  `apellido_medico` varchar(50) NOT NULL,
  `id_especialidad` int(11) NOT NULL,
  `fechaCreacion_medico` datetime NOT NULL,
  `fechaActualizacion_medico` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_medico`),
  KEY `id_especialidad` (`id_especialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pacientes`
--

CREATE TABLE IF NOT EXISTS `tbl_pacientes` (
  `id_paciente` int(11) NOT NULL,
  `historia_paciente` int(11) NOT NULL,
  `nombre_paciente` varchar(50) NOT NULL,
  `apellido_paciente` varchar(50) NOT NULL,
  `direccion_paciente` text NOT NULL,
  `sexo_paciente` enum('Femenino','Masculino') NOT NULL,
  `lugarNacimiento_paciente` varchar(50) NOT NULL,
  `fechaNacimiento_paciente` date NOT NULL,
  `edoCivil_paciente` enum('Soltero[a]','Casado[a]','Divorciado[a]','Viudo[a]','Otro') NOT NULL,
  `cedula_paciente` int(11) NOT NULL,
  `grupoSanguineo_paciente` varchar(10) NOT NULL,
  `religion_paciente` varchar(50) DEFAULT NULL,
  `claseEconomica_paciente` enum('Trabajador','Profesor','Estudiante','Obrero','Familiar de Trabajador','Familiar de Profesor','Familiar de Estudiante','Familiar de Obrero') NOT NULL,
  `profesion_paciente` varchar(75) NOT NULL,
  `ocupacion_paciente` varchar(75) NOT NULL,
  `direccionLaboral_paciente` text NOT NULL,
  `nombreFamiliar_paciente` varchar(50) NOT NULL,
  `parentescoFamiliar_paciente` varchar(50) NOT NULL,
  `direccionFamiliar_paciente` text NOT NULL,
  `fechaCreacion_paciente` date NOT NULL,
  `nombreEntrevistador_paciente` varchar(50) NOT NULL,
  `estado_paciente` enum('Activo','Pasivo') NOT NULL,
  `fechaActualizacion_paciente` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pacientes`
--

INSERT INTO `tbl_pacientes` (`id_paciente`, `historia_paciente`, `nombre_paciente`, `apellido_paciente`, `direccion_paciente`, `sexo_paciente`, `lugarNacimiento_paciente`, `fechaNacimiento_paciente`, `edoCivil_paciente`, `cedula_paciente`, `grupoSanguineo_paciente`, `religion_paciente`, `claseEconomica_paciente`, `profesion_paciente`, `ocupacion_paciente`, `direccionLaboral_paciente`, `nombreFamiliar_paciente`, `parentescoFamiliar_paciente`, `direccionFamiliar_paciente`, `fechaCreacion_paciente`, `nombreEntrevistador_paciente`, `estado_paciente`, `fechaActualizacion_paciente`) VALUES
(1, 523425, 'Johnmer', 'Bencomo', 'Urb. Cont', 'Masculino', 'Valera', '1980-06-18', 'Soltero[a]', 14460452, 'A+', 'Católico', 'Estudiante', 'hgasjdaks', 'askjdhjkas', 'lakjs lfksjd lfkjsdlfjls', 'asdasd', 'asdasdas', 'aj dlkja ldj lakjdl a', '0000-00-00', 'ias dj lakj dljalda', 'Activo', '2013-05-30 15:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(50) NOT NULL,
  `apellido_usuario` varchar(50) NOT NULL,
  `login_usuario` varchar(20) NOT NULL,
  `pwd_usuario` varchar(100) NOT NULL,
  `cnf_usuario` varchar(100) NOT NULL,
  `fechaRegistro_usuario` datetime NOT NULL,
  `fechaActualizacion_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `login_usuario`, `pwd_usuario`, `cnf_usuario`, `fechaRegistro_usuario`, `fechaActualizacion_usuario`) VALUES
(1, 'admin', 'admin', 'admin', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2013-05-29 00:00:00', '2013-05-29 16:49:14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 18, 2013 at 06:03 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_camiula`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_citas`
--

CREATE TABLE IF NOT EXISTS `tbl_citas` (
  `id_cita` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `fechaCreacion_cita` datetime NOT NULL,
  `fechaProgramada_cita` date NOT NULL,
  `fechaActualizacion_cita` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cita`),
  KEY `id_paciente` (`id_paciente`,`id_medico`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_citas`
--

INSERT INTO `tbl_citas` (`id_cita`, `id_paciente`, `id_medico`, `fechaCreacion_cita`, `fechaProgramada_cita`, `fechaActualizacion_cita`) VALUES
(1, 1, 8, '0000-00-00 00:00:00', '2013-06-07', '2013-06-18 16:57:19'),
(2, 3, 13, '0000-00-00 00:00:00', '2013-06-30', '0000-00-00 00:00:00'),
(3, 2, 6, '0000-00-00 00:00:00', '2013-06-08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_consultas`
--

CREATE TABLE IF NOT EXISTS `tbl_consultas` (
  `id_consulta` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `fechaCreacion_consulta` date NOT NULL,
  `tipo_consulta` enum('Citada','Emergencia','Cambio de Cita','Cortesía') NOT NULL DEFAULT 'Citada',
  `primera_consulta` enum('Si','No') NOT NULL,
  `diagnostico_consulta` text NOT NULL,
  `recipe_consulta` enum('Si','No') NOT NULL,
  `fechaActualizacion_consulta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_consulta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_consultas`
--

INSERT INTO `tbl_consultas` (`id_consulta`, `id_paciente`, `id_medico`, `fechaCreacion_consulta`, `tipo_consulta`, `primera_consulta`, `diagnostico_consulta`, `recipe_consulta`, `fechaActualizacion_consulta`) VALUES
(1, 0, 1, '2013-06-13', 'Emergencia', 'Si', 'sjhf ldjflksdjfkls lkjf lksjdfljsdljfd', 'No', '2013-06-13 16:29:50'),
(2, 2, 13, '2013-06-18', 'Citada', 'Si', 'fgdghj', 'No', '2013-06-18 19:43:39'),
(3, 2, 10, '2013-06-21', 'Cambio de Cita', 'No', 'ffgfgggh', 'No', '2013-06-18 16:49:36'),
(4, 3, 10, '2013-07-20', 'Citada', 'No', 'xgbfdhfgj', 'No', '2013-06-18 16:59:10'),
(5, 6, 10, '2013-05-16', 'Cortesía', 'Si', 'algo mas', 'No', '2013-06-18 20:53:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_especialidad`
--

CREATE TABLE IF NOT EXISTS `tbl_especialidad` (
  `id_especialidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_especialidad` varchar(50) NOT NULL,
  `fechaCreacion_especialidad` datetime NOT NULL,
  `fechaActualizacion_especialidad` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_especialidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_especialidad`
--

INSERT INTO `tbl_especialidad` (`id_especialidad`, `nombre_especialidad`, `fechaCreacion_especialidad`, `fechaActualizacion_especialidad`) VALUES
(1, 'Pediatría', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Medicina General', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Gineco Obstetricia', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Medicina Interna', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Odontología', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Nuutricionista', '2013-06-18 00:00:00', '0000-00-00 00:00:00'),
(7, 'Cirugía', '2013-06-18 00:00:00', '0000-00-00 00:00:00'),
(8, 'Reumatología', '2013-06-18 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_medicos`
--

CREATE TABLE IF NOT EXISTS `tbl_medicos` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_medico` varchar(50) NOT NULL,
  `apellido_medico` varchar(50) NOT NULL,
  `codigo_medico` varchar(10) NOT NULL,
  `id_especialidad` int(11) NOT NULL,
  `fechaCreacion_medico` datetime NOT NULL,
  `fechaActualizacion_medico` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_medico`),
  KEY `id_especialidad` (`id_especialidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_medicos`
--

INSERT INTO `tbl_medicos` (`id_medico`, `nombre_medico`, `apellido_medico`, `codigo_medico`, `id_especialidad`, `fechaCreacion_medico`, `fechaActualizacion_medico`) VALUES
(6, 'Dra. B.', 'Aranguibel', 'GIN', 3, '2013-06-18 00:00:00', '0000-00-00 00:00:00'),
(7, 'Dr Luis', 'Marquez', 'PED', 1, '2013-06-18 00:00:00', '2013-06-18 17:30:16'),
(8, 'Dra J', 'Gianfelice', 'MED', 2, '2013-06-18 00:00:00', '2013-06-18 17:30:34'),
(9, 'Dra N', 'Pacheco', 'MED', 2, '2013-06-18 00:00:00', '2013-06-18 17:30:46'),
(10, 'Dra Mercedes', 'Anónimo', 'NUT', 6, '2013-06-18 00:00:00', '2013-06-18 17:30:06'),
(11, 'Dr W', 'Aranguibel', 'MED', 4, '2013-06-18 00:00:00', '2013-06-18 17:30:59'),
(12, 'Dra Zulay', 'Flores', 'ODO', 5, '2013-06-18 00:00:00', '2013-06-18 17:31:11'),
(13, 'Dra Tirza', 'Lameda', 'ODO', 5, '2013-06-18 00:00:00', '2013-06-18 17:31:22'),
(14, 'Dr Ignacio', 'Lima', 'CIR', 7, '2013-06-18 00:00:00', '2013-06-18 17:31:38'),
(15, 'Dra A', 'Pacheco', 'REU', 8, '2013-06-18 00:00:00', '2013-06-18 15:58:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pacientes`
--

CREATE TABLE IF NOT EXISTS `tbl_pacientes` (
  `id_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `historia_paciente` int(11) NOT NULL,
  `nombre_paciente` varchar(50) NOT NULL,
  `apellido_paciente` varchar(50) NOT NULL,
  `direccion_paciente` text NOT NULL,
  `sexo_paciente` enum('Masculino','Femenino') NOT NULL,
  `lugarNacimiento_paciente` varchar(50) DEFAULT NULL,
  `fechaNacimiento_paciente` date DEFAULT NULL,
  `edoCivil_paciente` enum('soltero[a]','casado[a]','divorciado[a]','viudo[a]','otro') DEFAULT NULL,
  `cedula_paciente` int(11) DEFAULT NULL,
  `grupoSanguineo_paciente` varchar(10) DEFAULT NULL,
  `religion_paciente` varchar(50) DEFAULT NULL,
  `claseEconomica_paciente` enum('Trabajador','Profesor','Estudiante','Obrero','Familiar Trabajador','Familiar Profesor','Familiar Estudiante','Familiar Obrero') NOT NULL,
  `profesion_paciente` varchar(75) DEFAULT NULL,
  `ocupacion_paciente` varchar(75) DEFAULT NULL,
  `direccionLaboral_paciente` text,
  `nombreFamiliar_paciente` varchar(50) DEFAULT NULL,
  `parentescoFamiliar_paciente` varchar(50) DEFAULT NULL,
  `direccionFamiliar_paciente` text,
  `fechaCreacion_paciente` date DEFAULT NULL,
  `nombreEntrevistador_paciente` varchar(100) DEFAULT NULL,
  `estado_paciente` enum('Activo','Pasivo') NOT NULL DEFAULT 'Activo',
  `fechaActualizacion_paciente` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_paciente`),
  UNIQUE KEY `cedula_paciente_UNIQUE` (`cedula_paciente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_pacientes`
--

INSERT INTO `tbl_pacientes` (`id_paciente`, `historia_paciente`, `nombre_paciente`, `apellido_paciente`, `direccion_paciente`, `sexo_paciente`, `lugarNacimiento_paciente`, `fechaNacimiento_paciente`, `edoCivil_paciente`, `cedula_paciente`, `grupoSanguineo_paciente`, `religion_paciente`, `claseEconomica_paciente`, `profesion_paciente`, `ocupacion_paciente`, `direccionLaboral_paciente`, `nombreFamiliar_paciente`, `parentescoFamiliar_paciente`, `direccionFamiliar_paciente`, `fechaCreacion_paciente`, `nombreEntrevistador_paciente`, `estado_paciente`, `fechaActualizacion_paciente`) VALUES
(1, 1, 'Johnmer', 'Bencomo', 'jhgsjhg jhg fsgfsdhg fjsd jkfksd', 'Masculino', 'Valera', '1980-06-18', 'soltero[a]', 14460452, 'orh+', 'Catolica', 'Estudiante', 'adjghjksdh sdfsd', 'sdfdsfsdbn mbsdmfmds', 'kjh kfjsdh kjfh sdjk hfksdhfksd', 'jhs dkfjh sdjkhf ksd', 'sdfksdh fkhsdkfhksd', 'k sjdfh kjsdh kfhsdkhfsd kf', NULL, 'ksh kfsdh kfjhsd khfksd', 'Activo', NULL),
(2, 2, 'ana', 'jjjjjjjjjjjjjj', 'pampan', 'Femenino', 'trujillo', '2013-06-20', 'soltero[a]', 17200200, 'dfgf', 'dfgfdg', 'Estudiante', 'estud', 'estud', 'pampanito', 'jose', 'tio', 'pampan', NULL, 'nan', 'Activo', NULL),
(3, 3, 'antonio', 'fffffg', 'aaaaaaaaaaaa', 'Masculino', 'valera', '2013-06-26', 'casado[a]', 12000000, NULL, NULL, 'Obrero', 'obrero', 'obrero', 'obrero', 'maria', 'madre', 'jhjhjhjj', NULL, 'vivian', 'Activo', NULL),
(4, 66, 'jose', 'fffffff', 'ooooooooooo', 'Masculino', 'cerca', '2013-06-10', 'divorciado[a]', 15000900, NULL, NULL, 'Familiar Estudiante', 'albañil', 'aaaaaaaaa', 'kkkkkkkkkkk', 'ines', 'hermana', 'jjhhf', NULL, 'nan', 'Pasivo', NULL),
(5, 5, 'jose', 'caceres', 'valera', 'Femenino', 'valera', '2013-06-03', 'otro', 13400100, NULL, NULL, 'Profesor', 'profesor', 'docente', NULL, 'luis', 'padre', 'valera', NULL, 'vivian', 'Activo', NULL),
(6, 1, 'mari', 'peña', 'flor de patria', 'Femenino', 'trujillo', '2013-05-07', 'soltero[a]', 18100100, NULL, NULL, 'Familiar Estudiante', 'des', 'casa', 'casa', 'arturo', 'tio', 'pampan', NULL, 'nan', 'Activo', NULL),
(7, 2, 'fran', 'sulb', 'valera', 'Masculino', 'valera', '2013-01-03', 'soltero[a]', 1920300, NULL, NULL, 'Estudiante', 'estud', 'estud', NULL, 'ana', 'prima', 'valera', NULL, 'vivia', 'Activo', NULL),
(8, 910, 'luis', 'torres', 'pampan, el progrso. casaS/N', 'Masculino', 'trujillo', '2013-06-23', 'otro', 8, NULL, NULL, 'Familiar Obrero', 'comerciante', 'comerciante', 'valera', 'maria', 'hermana', 'pampan, trujillo', NULL, 'na', 'Pasivo', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usuario`
--

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(50) NOT NULL,
  `apellido_usuario` varchar(50) NOT NULL,
  `login_usuario` varchar(20) NOT NULL,
  `pwd_usuario` varchar(50) NOT NULL,
  `cnf_usuario` varchar(50) NOT NULL,
  `fechaRegistro_usuario` datetime NOT NULL,
  `fechaActualizacion_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `login_usuario`, `pwd_usuario`, `cnf_usuario`, `fechaRegistro_usuario`, `fechaActualizacion_usuario`) VALUES
(1, 'Administrador', 'Administrador', 'admin', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '2013-06-07 00:00:00', '2013-06-13 14:43:51'),
(9, 'Johnmer Alberto', 'Bencomo Caldera', 'bjohnmer', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '0000-00-00 00:00:00', '2013-06-18 17:09:49');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
